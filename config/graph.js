import { GraphQLClient } from 'graphql-request'

export const client = new GraphQLClient('https://api.graph.cool/simple/v1/cjtny91dl5n1f0158s51bos07', {
  headers: {
    Authorization: 'Bearer YOUR_AUTH_TOKEN',
  },
});