const UX = {
    'new component':'menu with labeled story icons.', 
    'day icon': 'circle with number inside indicating progress',
    'trigger icon': 'graph icon with action on it',
    'new screen': 'completion percentage of last seven days',
}