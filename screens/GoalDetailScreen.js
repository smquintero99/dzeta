import React from 'react';
import { ScrollView, StyleSheet, View} from 'react-native';
import { Text, Overlay, Input, CheckBox, Button  } from 'react-native-elements';
import { Achievement } from '../components/Achievement'
import { client } from '../config/graph'

export default class GoalDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      achievements:[],
      isVisible: false,
      achievement_name: '',
      completed_achievements: []
    }
  }

  static navigationOptions = {
    header: null,
  }

  componentWillMount = () => {
    this.fetchAchievements(false)
    this.fetchAchievements(true)
  }

  fetchAchievements = (completed) => {
    return client.request(`
      {
        allAchievements(
          filter: {
            goal:{
              id:"${this.props.navigation.state.params.goalId}"
            }
            completed:${completed}
          }
        ){
          id
          name
          completed
        }
        
      }
    `)
    .then( response => 
      !completed
        ? this.setState({ achievements: response.allAchievements })
        : this.setState({ completed_achievements: response.allAchievements }))
    .catch(console.log)
  }
  
  handleSwitch = ( id ) => { 
    const new_achievement = this.reverseAchievement( id )
    this.updateAchievement( id, new_achievement.completed)
    this.updateGoal()
  }

  updateGoal = () => {
    return client.request(`mutation
      {
        updateGoal(
          id:"${this.props.navigation.state.params.goalId}"
          progress:${ Math.random() }
        ){
          updatedAt
        }
      }
    `)
    .catch(e=>console.log(e))
  }

  reverseAchievement = (id) => {
    const new_achievement = { 
      ...this.state.achievements.find(achievement => achievement.id === id), 
      completed: !this.state.achievements.find(achievement => achievement.id === id).completed
    }
    const achievements = this.state.achievements.map(achievement => achievement.id===id ? new_achievement : achievement)
    this.setState({ achievements: achievements })
    return new_achievement
  }

  createAchievement = () => {
    return client.request(`
      mutation{
        createAchievement(
          name:"${this.state.achievement_name}"
          goalId:"${this.props.navigation.state.params.goalId}"
          completed:false
        ){
          id
          name
          completed
        }
      }
    `)
    .then(response => {
      this.setState({ achievements: [...this.state.achievements, response.createAchievement], isVisible:false})
      this.updateGoal()
    })
    .catch(console.log)
  }

  updateAchievement = ( id, completed ) => {
    client.request(`
      mutation{
        updateAchievement(
          id:"${id}"
          completed: ${completed}
        ){
          id
        }
      }
    `)
    .catch(()=>this.reverseAchievement(id))
  }

  render() {
    const submitButton = this.state.achievement_name.length > 0
      ? <Button 
          title="Create Achievement" 
          type="outline" 
          onPress={this.createAchievement} 
          containerStyle={{marginTop:20}}
        />
      : null

    const closeGoal = 
      this.state.achievements.filter(achievement=>!achievement.completed).length === 0 &&
      this.state.completed_achievements.filter(achievement=>!achievement.completed).length === 0 
        ? <CheckBox 
            center 
            title='Goal Achieved' 
            checked={this.state.checked}
            onPress={() => this.setState({checked: !this.state.checked})}
          />
        : null
      

    const overlay = 
      <Overlay isVisible={this.state.isVisible} height={'40%'}>
        <View>
          <Input
            placeholder='What are you achieving?'
            rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            onChangeText={(text) => this.setState({achievement_name: text})}
            onBackdropPress={ () => this.setState({isVisible:false}) }
          />
          { submitButton }
        </View>
      </Overlay>

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text h3 h3Style={{textAlign: 'center', color:'#224466'}}> Goal Details </Text>
          { closeGoal }
          {this.state.achievements.map((achievement, key)=> 
            <Achievement 
              title={achievement.name} 
              completed={achievement.completed} 
              id={achievement.id}
              handleSwitch={this.handleSwitch}
              key={key}
            />
          )}
          <Button
            title="Add Achievement"
            type="outline"
            onPress = {() => this.setState({isVisible:true})}
          />
          { overlay }
          {this.state.completed_achievements.map((achievement, key)=> 
            <Achievement 
              title={achievement.name} 
              completed={achievement.completed}
              id={achievement.id}
              handleSwitch={this.handleSwitch}
              key={key}
            />
          )}
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
});
