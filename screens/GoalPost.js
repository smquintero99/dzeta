import React from 'react';
import { ScrollView, StyleSheet, View} from 'react-native';
import { Text, Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { client } from '../config/graph'

const input_style = {
  height: 40,
  paddingLeft: 6,
  selectionColor: '#428AF8',
  underlineColorAndroid: '#428AF8',
}
export default class GoalPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        goal_icon:'',
        goal_name:''
    }
  }

  static navigationOptions = {
    header: null,
  }

  submitGoal = () => {
    return client.request(`
      mutation{
        createGoal(
            name:"${this.state.goal_name}"
            icon:"${this.state.goal_icon}"
        ){
            id
        }
      }
    `)
    .then(() => this.props.navigation.navigate('Goal', {}))
    .catch(console.log)
  }

  render() {
    const submitButton = this.state.goal_icon.length > 0 && this.state.goal_name.length > 0
      ? <Button title="Create New Goal" type="outline" onPress={this.submitGoal}/>
      : null
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text h3 h3Style={{textAlign: 'center', color:'#224466'}}> Create New Goal </Text>
          <Input
            style = {input_style}
            placeholder='Name of the Goal'
            rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            onChangeText={(text) => this.setState({goal_name: text})}
          />
          <Input
            style = {input_style}
            placeholder='Icon of the goal: live-help'
            rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            onChangeText={(text) => this.setState({goal_icon: text})}
          />
          { submitButton }
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
});
