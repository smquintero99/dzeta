import React from 'react';
import { Picker, ScrollView, StyleSheet, View, Alert } from 'react-native';
import { Button,  ButtonGroup, Input, ListItem, Overlay, Text } from 'react-native-elements';
import { client } from '../config/graph'

const income_label = 'Add Inconme'
const expense_label = 'Add Expense'
const buttons = [income_label, expense_label]
export default class FinanceScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible:false,
      expenses:[],
      selectedIndex: null,
      expense_type:null,
      expense_types:[
        '',
        'Rent',
        'Fixed',
        'Taxes',
        'Variable',
        'Leisure'
      ],
      description: '',
      quantity: 0,
      posted: false
    }
    this.updateIndex = this.updateIndex.bind(this)
  }

  static navigationOptions = {
    header: null
  }

  updateIndex = (selectedIndex) => {
    this.setState({selectedIndex:selectedIndex, isVisible:true})
  }

  save = () => {
    const description = this.state.description
    const quantity = this.state.quantity
    this.setState({
      isVisible:false, 
      expenses:[...this.state.expenses, {
        description:description, 
        quantity: this.state.selectedIndex === 0 ? Number(quantity) : Number(0 - quantity)
      }],
      description: '',
      quantity: 0,
      selectedIndex: null,
      expense_type:null
    })
  }

  postBalance = () => {
    let incomes = ""
    this.state.expenses.forEach(income => incomes += (
      '{name:"' + income.description + 
      '", quantity:'+ income.quantity + 
      '}'))
    console.log(this.state.expenses)
    console.log(incomes)
    client.request(`mutation
      {
        createBalance(
          incomes: [${incomes}]
          balance: ${this.state.expenses.reduce((a, b) => ({quantity: a.quantity + b.quantity})).quantity}
        ){
          id
        }
      }
    `)
    .then(_=> this.setState({ posted:true }))
    .catch(console.log)
  }

  submitBalance = () => {
    Alert.alert(
      'Submit Balance',
      "Today's balance stands at: $" + this.state.expenses.reduce((a, b) => ({quantity: a.quantity + b.quantity})).quantity,
      [
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        {text: 'OK', onPress: () => this.postBalance()}
      ]
    )    
  }

  render() {

    const submitButton = !this.state.posted 
      ? <Button title="Submit Balance" type="outline" onPress={this.submitBalance}/>
      : null

    const modalButton = this.state.description.length > 0 && this.state.quantity > 0
      ? <Button title="Save" type="outline" onPress={ () => this.save() }/>
      : null

    const expense_type = 
      <View>
        <Text>Expense Label</Text>
        <Picker
          style={{width: '100%'}}
          selectedValue={this.state.expense_type}
          onValueChange={(expense_type) => this.setState({expense_type: expense_type})}>
          {
            this.state.expense_types.map((expense, key) =>
              <Picker.Item label={expense} value={expense} key={key}/>
            )
          }
        </Picker>
      </View>
    
    const overlay = 
      <Overlay isVisible={this.state.isVisible} height={'60%'}>
        <View>
          <Input
            placeholder='Brief Description'
            rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            onChangeText={(text) => this.setState({description: text})}
          />
          <Input
            placeholder='Quantity'
            keyboardType = 'numeric'
            rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            onChangeText={(text) => this.setState({quantity: text})}
          />
          { buttons[this.state.selectedIndex] === income_label
            ? null
            : expense_type
          }
          { modalButton }
        </View>
      </Overlay>

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text h3 h3Style={{textAlign: 'center', color:'#224466'}}> Finance Tracker </Text>
          <ButtonGroup 
            onPress={this.updateIndex}
            selectedIndex={this.state.selectedIndex}
            buttons={buttons}
          />
          {
            this.state.expenses.map((expense, key)=>
              <ListItem 
                title = { expense.description }
                key = { key }
                badge={{ value: expense.quantity, textStyle: { color: 'orange' }, containerStyle: { marginTop: -20 } }}
              />
            )
          }

          {
            this.state.expenses.length > 0 
              ? <ListItem 
                  title = 'Daily Total'
                  badge={{ 
                    value: this.state.expenses.reduce((a, b) => ({quantity: a.quantity + b.quantity})).quantity,
                    textStyle: { color: 'orange' }, 
                    containerStyle: { marginTop: -20 } 
                  }}
                />
              : null
          }
          { submitButton }
          { overlay }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
})
