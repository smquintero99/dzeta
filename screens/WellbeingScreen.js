import React from 'react';
import { ScrollView, StyleSheet, View, Alert } from 'react-native';
import { Text, Button } from 'react-native-elements';
import { Achievement } from '../components/Achievement';
import { client } from '../config/graph'

export default class WellbeingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      achievements:[
        { id:1, title:'7:30 Thankfulness breathing', completed:false },
        { id:2, title:'7:45 Exercise and fruit breakfast', completed:false },
        { id:3, title:'8:45 Bath, teeth brushing and clothing', completed:false },
        { id:4, title:'9:00 Work', completed:false },
        { id:5, title:'13:00 Eat and teeth brushing', completed:false },
        { id:6, title:'14:00 Work', completed:false },
        { id:7, title:'18:00 Cultivate relationships and spazieren', completed:false },
        { id:8, title:'19:00 Personal Projects Engineering', completed:false },
        { id:9, title:'21:00 Learn', completed:false },
        { id:10, title:'22:00 Personal Projects Design', completed:false },
        { id:11, title:'23:00 Teeth brushing, thankfulness breathing and personal inventory', completed:false },
        { id:12, title:'23:15 Leisure', completed:false },
        { id:13, title:'00:00 Sleep', completed:false }
      ],
      posted: false
    }
  }

  static navigationOptions = {
    header: null,
  }

  reverseAchievement = (id) => {
    const new_achievement = { 
      ...this.state.achievements.find(achievement => achievement.id === id), 
      completed: !this.state.achievements.find(achievement => achievement.id === id).completed
    }
    const achievements = this.state.achievements.map(achievement => achievement.id===id ? new_achievement : achievement)
    this.setState({ achievements: achievements })
    return new_achievement
  }
  
  handleSwitch = ( id ) => {
    this.reverseAchievement( id )
  }

  postRoutine = () => {
    let habits = ""
    this.state.achievements.forEach(habit => habits += ('{name:"' + habit.title + '",completed:'+ habit.completed + '}'))
    client.request(`mutation
      {
        createRoutine(
          habits: [${habits}]
        ){
          id
          createdAt
          habits{
            name
            completed
          }
        }
      }
    `)
    .then(_=> this.setState({ posted:true }))
    .catch(console.log)
  }

  submitRoutine = () => {
    Alert.alert(
      'Submit Routine',
      'You completed ' + 
      String(this.state.achievements.filter(achievement=> achievement.completed).length) + '/' +  
      String(this.state.achievements.length) + ' daily tasks',
      [
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        {text: 'OK', onPress: () => this.postRoutine()}
      ]
    )
  }

  render() {
    const submitButton = !this.state.posted 
      ? <Button title="Submit Routine" type="outline" onPress={this.submitRoutine}/> 
      : null
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text h3 h3Style={{textAlign: 'center', color:'#224466'}}> Wellbeing </Text>
          {this.state.achievements.map((achievement, key)=> 
            <Achievement 
              key = { key } 
              id = { achievement.id }
              title = { achievement.title } 
              completed = { achievement.completed }
              handleSwitch = { this.handleSwitch }
            />
          )}
          { submitButton }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
});
