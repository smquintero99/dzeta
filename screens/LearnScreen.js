import React from 'react';
import { ScrollView, StyleSheet, View} from 'react-native';
import { Text, Divider, Overlay, Input, Button } from 'react-native-elements';
import { Achievement } from '../components/Achievement'
import OverlayPost from '../components/OverlayPost'
import { Book } from '../components/Book'
import { client } from '../config/graph'

export default class LearnScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      achievements:[
        {
          id:1,
          title:'Data Science',
          completed:false
        },
      ],
      isVisible: false,
      learned_description: false,
      books:[],
      placeholder:'',
      submit_text:'',
      overlay_action:'',
      updated_book:null
    }
  }
  
  static navigationOptions = {
    header: null
  }

  componentDidMount(){
    this.fetchBooks()
  }

  fetchBooks = () => {
    return client.request(`
      {
        allBooks(
          orderBy: updatedAt_DESC 
        ){
          id
          name
          progress
        }
      }
    `)
    .then(response => this.setState({ books: response.allBooks }))
    .catch(console.log)
  }

  reverseAchievement = (id) => {
    const new_achievement = { 
      ...this.state.achievements.find(achievement => achievement.id === id), 
      completed: !this.state.achievements.find(achievement => achievement.id === id).completed
    }
    const achievements = this.state.achievements.map(achievement => achievement.id===id ? new_achievement : achievement)
    this.setState({ achievements: achievements })
    return new_achievement
  }

  handleSwitch = ( id ) => {
    const new_achievement = this.reverseAchievement( id )
    new_achievement.completed 
      ? this.setState({ 
          isVisible: true, 
          placeholder:'What did you learned?', 
          submit_text:'Record Learning',
          overlay_action: 'record_study'  
        }) 
      : null
  }

  updateSlider = ( id, progress ) => { 
    const book = this.state.books.find(book => book.id === id)
    progress > book.progress 
      ? setTimeout(() => {
          this.setState({  
            isVisible: true, 
            placeholder: 'What did you learned?', 
            submit_text: 'Record Learning', 
            overlay_action: 'update_book' 
          })
        }, 1200) 
      : null
    const books = this.state.books.map(book => book.id===id ? {...book, progress:progress} : book)
    this.setState({ books: books, updated_book:id })
  }

  recordStudy = () => {
    return client.request(`mutation
      {
        createStudy(
          name:"${this.state.achievements[0].title}"
          learned:"${this.state.learned_description}"
        ){
          id
        }
      }
    `)
    .catch(console.log)
  }

  createBook = () => {
    return client.request(`mutation
      {
        createBook(
          name:"${this.state.learned_description}"
          progress:0
          learned:[]
        ){
          id
          name
          progress
        }
      }
    `)
    .then(response => this.setState({ 
      books: [ response.createBook, ...this.state.books], 
    }))
    .catch(console.log)    
  }

  updateBook = () => {
    const book = this.state.books.find(book => book.id === this.state.updated_book)
    client.request(`mutation
      {
        updateBook(
          id:"${book.id}"
          progress:${Number(book.progress)}
        ){
          id
        }
      }
    `)
    .catch(console.log)    
    client.request(`mutation
      {
        createLearn(
          bookId:"${book.id}"
          learned:"${this.state.learned_description}"
        ){
          id
        }
      }
    `)
    .catch(console.log)    
  }

  handleSubmit = () => {
    this.setState({isVisible:false})
    switch (this.state.overlay_action) {
      case 'record_study': return this.recordStudy()
      case 'create_book': return this.createBook()
      case 'update_book': return this.updateBook()
      default: null
    }
  }

  onChangeText = (text) => {
    this.setState({ learned_description: text })
  }

  render() {

    const submitButton = this.state.learned_description.length > 0
      ? <Button title={this.state.submit_text} type="outline" onPress={ () => this.handleSubmit() }/>
      : null

    const overlay = 
      <Overlay isVisible={this.state.isVisible} height={'40%'}>
        <View>
          <Input
            placeholder={this.state.placeholder}
            rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
            onChangeText={(text) => this.setState({learned_description: text})}
          />
          { submitButton }
        </View>
      </Overlay>

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text h3 h3Style={{textAlign: 'center', color:'#224466'}}> Study Material </Text>
          {this.state.achievements.map((achievement, key)=> 
            <Achievement 
              key = { key } 
              id = { achievement.id }
              title = { achievement.title } 
              completed = { achievement.completed }
              handleSwitch = { () => this.handleSwitch(achievement.id) }
            />
          )}
          <Divider style={{ backgroundColor: 'blue' }} />
          <Button
            title="Add Book"
            type="outline"
            onPress = {() => this.setState({
              isVisible:true, 
              placeholder:'Book Name', 
              submit_text:'Create Book',
              overlay_action: 'create_book' 
            })}
          />
          { 
            this.state.books.map( book => 
              <Book 
                book = {book} 
                key = {book.id} 
                updateSlider = {this.updateSlider}
              />
            ) 
          }
          <OverlayPost 
            isVisible = { this.state.isVisible }
            input_value = { this.state.learned_description }
            onChangeText = { this.onChangeText }
            handleSubmit = { this.handleSubmit } 
            textButton = { this.state.submit_text }
            placeholder = { this.state.placeholder }
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
});
