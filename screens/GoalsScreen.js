import React from 'react';
import { ScrollView, StyleSheet, View} from 'react-native';
import { Text, Image } from 'react-native-elements';
import { GoalIcon } from '../components/GoalIcon'
import { client } from '../config/graph'

export default class GoalsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      goals : [],
    }
  }

  static navigationOptions = {
    header: null,
  }

  componentWillMount = () => {
    this.fetchGoals()
  }

  fetchGoals = () => {
    return client.request(`
      {
        allGoals(
          orderBy:updatedAt_DESC
        ){
          id
          icon
          name          
        }
      }
    `)
    .then( response => this.setState({goals: split([...response.allGoals,{icon:'add', name:'Add'}], 3)} ))
    .catch(console.log)
  }

  render() {
    const { navigate } = this.props.navigation;
    const goals = 
    this.state.goals.map((row,idx)=>
      <View style={{ flexDirection: 'row', flex: 1, alignItems: 'stretch', justifyContent: 'space-between'}} key={idx}>
        {row.map((icon, key) => 
          <GoalIcon 
            icon={icon.icon} 
            name={icon.name} 
            color = {
              icon.name !== 'Add' 
              ? '#003377'
              : '#770300'
            }
            navigate={() => 
              icon.name !== 'Add' 
              ? navigate('GoalDetail', {goalId:icon.id})
              : navigate('GoalPost', {})
            } 
            key={key}
          />
        )}
      </View>
      )

      return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Image 
            source={require('../assets/images/goalsTitle.png')} 
            style={{ width: '80%', marginLeft:'10%', resizeMode:'contain' }}
          />        
          { goals }
        </ScrollView>
      </View>
    );
  }

}

const split = (arr, n) => {
  var res = [];
  while (arr.length) {
    res.push(arr.splice(0, n));
  }
  return res;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
});
