import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Text, Button } from 'react-native-elements';
import { Achievement } from '../components/Achievement'
import OverlayPost from '../components/OverlayPost'
import { client } from '../config/graph'

export default class GoalDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      achievements:[],
      isVisible:false,
      new_mit_name:''
    }
  }

  static navigationOptions = {
    header: null,
  }

  componentDidMount() {
    this.fetchMITs()
  }

  fetchMITs = () => {
    return client.request(`
      {
        allMITs(
          orderBy: createdAt_DESC
        ){
          id
          name
          completed
        }
      }
    `)
    .then(response => this.setState({achievements: response.allMITs }))
    .catch(console.log)
  }

  createMIT = () => {
    return client.request(`mutation
      {
        createMIT(
          name:"${this.state.new_mit_name}"
          completed:false
        ){
          id
          name
          completed
        }
      }
    `)
    .then(response => this.setState({ 
      achievements: [ response.createMIT, ...this.state.achievements], 
      isVisible: false
    }))
    .catch(console.log)
  }
  
  reverseAchievement = (id) => {
    const new_achievement = { 
      ...this.state.achievements.find(achievement => achievement.id === id), 
      completed: !this.state.achievements.find(achievement => achievement.id === id).completed
    }
    const achievements = this.state.achievements.map(achievement => achievement.id===id ? new_achievement : achievement)
    this.setState({ achievements: achievements })
    return new_achievement
  }
  
  updateAchievement = ( id, completed ) => {
    client.request(`
      mutation{
        updateMIT(
          id:"${id}"
          completed: ${completed}
        ){
          id
        }
      }
    `)
    .catch(()=>this.reverseAchievement(id))
  }

  handleSwitch = ( id ) => { 
    const new_achievement = this.reverseAchievement( id )
    this.updateAchievement( id, new_achievement.completed)
  }

  onChangeText = (text) => {
    this.setState({new_mit_name: text})
  } 

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Text h3 h3Style={{textAlign: 'center', color:'#224466'}}> Most Important Thing </Text>
          <Button
            title="Add MIT"
            type="outline"
            onPress = {() => this.setState({isVisible:true})}
          />
          {this.state.achievements.map((mit, key)=> 
            <Achievement 
              title={mit.name} 
              completed={mit.completed} 
              id={mit.id}
              handleSwitch={this.handleSwitch}
              key={key}          
            />
          )}
          <OverlayPost 
            isVisible={this.state.isVisible}
            input_value={this.state.new_mit_name}
            onChangeText={this.onChangeText}
            handleSubmit={this.createMIT} 
            textButton = {'Add MIT'}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
})
