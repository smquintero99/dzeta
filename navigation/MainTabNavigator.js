import React from 'react'
import { Platform } from 'react-native'
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation'

import TabBarIcon from '../components/TabBarIcon'
import GoalsScreen from '../screens/GoalsScreen'
import GoalDetailScreen from '../screens/GoalDetailScreen'
import GoalPost from '../screens/GoalPost'
import MITScreen from '../screens/MITScreen'
import LearnScreen from '../screens/LearnScreen'
import WellbeingScreen from '../screens/WellbeingScreen'
import FinanceScreen from '../screens/FinanceScreen' 

const GoalStack = createStackNavigator({
  Goal: GoalsScreen,
  GoalDetail: {screen: GoalDetailScreen},
  GoalPost: {screen: GoalPost}
})

GoalStack.navigationOptions = {
  tabBarLabel: 'Goals',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-arrow-dropdown${focused ? '' : '-circle'}`
          : 'md-checkmark-circle'
      }
    />
  ),
}

const MITStack = createStackNavigator({
  MIT: MITScreen,
})

MITStack.navigationOptions = {
  tabBarLabel: 'MIT',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-information-circle' : 'md-information-circle'}
    />
  ),
}

const LearnStack = createStackNavigator({
  Learn: LearnScreen,
})

LearnStack.navigationOptions = {
  tabBarLabel: 'Learn',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'}
    />
  ),
}

const WellbeingStack = createStackNavigator({
  Wellbeing: WellbeingScreen,
})

WellbeingStack.navigationOptions = {
  tabBarLabel: 'Wellbeing',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-hammer' : 'md-hammer'}
    />
  ),
}

const FinanceStack = createStackNavigator({
  Finance: FinanceScreen,
})

FinanceStack.navigationOptions = {
  tabBarLabel: 'Finance',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-sunny' : 'md-sunny'}
    />
  ),
}

export default createBottomTabNavigator({
  GoalStack,
  MITStack,
  LearnStack,
  WellbeingStack,
  FinanceStack
})
