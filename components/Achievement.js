import React from 'react';
import {ListItem} from 'react-native-elements'

export class Achievement extends React.Component {
    render() {
    return (
        <ListItem
            title={this.props.title}
            subtitle = {this.props.subtitle}
            switch = {{ 
                onValueChange: () => this.props.handleSwitch(this.props.id), 
                value: String(this.props.completed) === 'true' ? true : false
            }} 
        />
    )
  }
}
