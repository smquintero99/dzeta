import React from 'react';
import { View } from 'react-native';
import { Text, Slider } from 'react-native-elements';

export class Book extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            progress:this.props.book.progress
        }
    }    

    updateSlider = (progress) => { 
        this.props.updateSlider(this.props.book.id, progress)
        this.setState({ progress: progress }) 
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center' }}>
                <Slider
                    value = { this.state.progress/100 }
                    onValueChange = { progress => this.updateSlider(Math.round(progress * 100))}
                />
                <Text>{this.props.book.name + ': ' + this.state.progress + '%' }</Text>
          </View>
        )
    }
}
