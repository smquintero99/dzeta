import React from 'react'
import { View } from 'react-native'
import { Overlay, Input, Button } from 'react-native-elements'

const OverlayPost = ({isVisible, input_value, onChangeText, handleSubmit, placeholder, textButton}) => (
  <Overlay isVisible={isVisible} height={'40%'}>
    <View>
      <Input
        placeholder= { placeholder || 'What are you achieving?' }
        rightIcon={{ type: 'font-awesome', name: 'chevron-left' }}
        onChangeText={(text) => onChangeText(text)}
      />
      { 
        input_value.length > 0
          ? <Button 
              title={ textButton } 
              type="outline" 
              onPress={handleSubmit}
              containerStyle={{marginTop:20}}
            />
          : null 
      }
    </View>
  </Overlay>
)

export default OverlayPost