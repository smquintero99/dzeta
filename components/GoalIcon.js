import React from 'react'
import { View } from 'react-native'
import { Icon, Text } from 'react-native-elements'

export class GoalIcon extends React.Component {
    render() {
    return (
        <View style={{margin:20}}>
            <Icon name={this.props.icon} size={67} color={this.props.color} onPress={()=>this.props.navigate()}/>
            <Text h4 h4Style={{textAlign: 'center', fontSize: 23, color:this.props.color}}>{this.props.name}</Text>
        </View>
    )
  }
}
